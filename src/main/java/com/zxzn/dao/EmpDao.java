package com.zxzn.dao;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zxzn.entity.EmpEntity;
import lombok.Data;
import org.apache.ibatis.annotations.Mapper;


@Mapper
public interface EmpDao extends BaseMapper<EmpEntity> {


}
