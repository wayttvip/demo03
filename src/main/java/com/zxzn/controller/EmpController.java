package com.zxzn.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zxzn.entity.EmpEntity;
import com.zxzn.service.EmpService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Api(value = "UserController", tags = {"用户操作接口"})
@Controller
@RequestMapping("/emp")
public class EmpController {

    @Autowired
    private EmpService empService;


    @ApiOperation(value = "index", tags = {"进入查询操作"})
    @GetMapping("/index")
    public String toIndex(){
        return "index";
    }

    @Value("${pageSize}")
    private Integer pageSize;

    @ApiOperation(value = "findAll", tags = {"查询用户信息"})
    @GetMapping("/findAll")
    @ResponseBody
    public String toFindAll(Model model, @RequestParam(value = "pageNumber", defaultValue = "1") int pageNumber){
        System.out.println(pageNumber);
        System.out.println(pageSize);
        IPage emps = empService.page(new Page<>( pageNumber, pageSize));
        List<EmpEntity> empEntities = emps.getRecords();
        System.out.println(pageNumber);
        System.out.println(pageSize);
        model.addAttribute("empEntities", empEntities);
      /*  return empEntities;*/
        return "list";
    }

    @ApiOperation(value = "toUpdate", tags = {"跳转到修改页面"})
    @GetMapping("/toUpdate")
    public String toUpdate(int id, Model model){
        EmpEntity empEntity = empService.getById(id);
        System.out.print(empEntity);
        model.addAttribute("empEntity", empEntity);
        return "update";
    }

    @ApiOperation(value = "update", tags = {"修改用户信息"})
    @PostMapping("/update")
    public String update(EmpEntity empEntity){
        System.out.print( "empEntity" + empEntity);
        empService.updateById(empEntity);
        return "redirect:findAll";
    }

    @ApiOperation(value = "toAdd", tags = {"跳转到用户添加页面"})
    @RequestMapping("/toAdd")
    public String toAdd(){
        return "add";
    }

    @ApiOperation(value = "add", tags = {"添加用户信息"})
    @PostMapping("/add")
    public String add(EmpEntity empEntity){
        empService.save(empEntity);
        return "redirect:findAll";
    }

    @ApiOperation(value = "delete", tags = {"删除用户信息"})
    @GetMapping("/delete")
    public String delete(int id){
        empService.removeById(id);
        return "redirect:findAll";
    }
}
